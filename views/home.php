<?php
include __DIR__ . '/includes/header.php';
?>

<!--        <!-- BEGIN ABOUT SECTION -->
<?php
include __DIR__ . '/includes/about.php';
?>
<!--        <!-- END ABOUT SECTION -->

<!--        <!-- BEGIN TIMETABLE SECTION -->
<?php
include __DIR__ . '/includes/time.php';
?>
<!--        <!-- END TIMETABLE SECTION -->

<!--        <!-- BEGIN MENU SECTION -->
<?php
include __DIR__ . '/includes/menu.php';
?>
<!--        <!-- END MENU SECTION -->

<!--        <!-- BEGIN GALLERY SECTION -->
<?php
include __DIR__ . '/includes/gallery.php';
?>
<!--        <!-- END GALLERY SECTION -->

<!--        <!-- BEGIN RESERVATIONS SECTION -->
<?php
include __DIR__ . '/includes/reservations.php';
?>
<!--        <!-- END RESERVATIONS SECTION-->

<!--        <!-- BEGIN TESTIMONIALS SECTION -->
<?php
include __DIR__ . '/includes/testimonials.php';
?>

<!-- END TESTIMONIALS SECTION -->

<!--        <!-- BEGIN CONTACT SECTION -->
<?php
include __DIR__ . '/includes/contact.php';
?>

<!--        <!-- END CONTACT SECTION -->

<!--        <!-- BEGIN MAP SECTION -->
<?php
include __DIR__ . '/includes/map.php';
?>

<!--        <!-- END MAP SECTION -->