<title>Restoran Promaja</title>
<!-- GOOGLE FONTS -->
<link href='http://fonts.googleapis.com/css?family=Yellowtail%7cCabin:400,500,600,700,400italic,700italic%7cLibre+Baskerville:400italic%7cGreat+Vibes%7cOswald:400,300,700%7cOpen+Sans:600italic,700' rel='stylesheet' type='text/css'>

<!-- META TAGS -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="robots" content="index, follow" />

<!-- CSS STYLESHEETS -->
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/font-awesome.min.css" />
<link rel="stylesheet" href="css/elixir.css" />
<link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="js/owl-carousel/owl.theme.css" rel="stylesheet">
<link href="js/owl-carousel/owl.transitions.css" rel="stylesheet">
<link href="css/YTPlayer.css" rel="stylesheet">
<link rel="stylesheet" href="css/swipebox.css">


<!--[if lt IE 9]>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<script src="./js/html5shiv.js"></script>
<script src="./js/respond.js"></script>
<![endif]-->
