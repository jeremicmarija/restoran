<!-- BEGIN Scripts-->

<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.js"></script>
<script src="js/classie.js"></script>
<script src="js/pathLoader.js"></script>
<script src="js/owl-carousel/owl.carousel.min.js"></script>
<script src="js/jquery.inview.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/default.js"></script>
<script src="js/plugins.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.swipebox.js"></script>
<script src="js/form.min.js"></script>
<script src="js/sendmsg.js"></script>
<script src="js/logo.js"></script>
<script type="text/javascript">
    ;( function( $ ) {

        $( '.swipebox' ).swipebox();

    } )( jQuery );
</script>


<!-- END Scripts -->