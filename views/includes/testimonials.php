<!-- BEGIN TESTIMONIALS SECTION -->
<section id="testimonials" class="section testimonials parallax  clearfix">

    <div class="container">
        <div class="jt_row jt_row-fluid row">
          <!--   <div class="voffset20"></div> -->
            <div class="col-md-12 jt_col column_container">

                <h2 class="section-title no-side-lines">Utisci naših gostiju</h2>
            </div>


            <div class="col-md-12 jt_col column_container ">
                <div class="testimonials carousel-wrapper with_pagination">
                    <div class="owl-carousel generic-carousel">
                      <div class="item">
                        <p>"Nice place, just next to Sava river. Prices are average, I would say even a bit lower for such a location! The space is divided into smoking and non-smoking areas. There is a kitchen, offering both traditional but also international (italian) cuisine! Very nice garden, with a beautiful view over the river and New Belgrade. "</p>
                        <span class="author"> Filip Petrović</span>
                      </div>
                        <div class="item">
                            <p>"Dobra usluga, pogled, muzika sve u svemu savršeno mesto za izlazak ."</p>
                            <span class="author">Dejan Petrović</span>
                        </div>
                        <div class="item">
                            <p>"Very nice place for Lunch in the afternoon. Beer selection is very good. The blonde lady  is very friendly and professional. Very funk and classic decoration inside. Well recommended. "</p>
                            <span class="author">Paulo Coutinho</span>
                        </div>
                        <div class="item">
                            <p>"Prijatna atmosfera, lepo uređenje, odlična klopa."</p>
                            <span class="author">Aleksandra Minić</span>
                        </div>
                    </div>
                </div>
                <div class="voffset80"></div>
            </div>
        </div>
    </div> <!-- End .container -->
</section>
<!-- END TESTIMONIALS SECTION -->
