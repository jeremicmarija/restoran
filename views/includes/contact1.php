<!-- BEGIN CONTACT SECTION -->
<section id="contact" class="section contact dark">
    <div class="container">
        <div class="jt_row jt_row-fluid row">

            <div class="col-md-12 jt_col column_container">
                <h2 class="section-title">Kontakt</h2>
            </div>
            <div class="col-md-6 col-md-offset-3 jt_col column_container">
                <div class="section-subtitle">
                    <p>Restoran Promaja, Karađorđeva 8a, Beograd</p>
                    <p>Informacije i rezervacije: 011/262-80-96 ; 064/8-666-660</p>
                    <p>E-mail : info@restoranpromaja.rs</p>
                </div>
            </div>
            <form action="/newversion/mail.php" method="post" id="contactform" class="contact-form">
                <div class="col-md-6 jt_col column_container">
                    <input type="text" id="name" name="name" class="text name required" placeholder="Ime" >
                    <input type="email" id="email" name="email" class="tex email required" placeholder="Email" >
                    <input type="text" id="subject" name="subject" placeholder="Naslov" >
                </div>

                <div class="col-md-6 jt_col column_container">
                    <textarea id="message" name="textmessage" class="text area required" placeholder="Poruka" rows="10"></textarea>
                </div>

                <div class="col-md-4 col-md-offset-4 jt_col column_container">
                    <div id="msg-status" class="formSent"></div>
                    <input type="submit" class="button contact center" id="sendmsg" value="Pošalji" >
                </div>

            </form>
            <div class="voffset100"></div>
        </div>
    </div>

</section>
<!-- END CONTACT SECTION -->
