<!-- BEGIN FOOTER -->
<footer id="footer" class="section" role="contentinfo">
    <div class="container">
        <ul class="social">
            <li><a href="https://www.facebook.com/Promajalb/?ref=bookmarks" class="icon fb" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.instagram.com/restoran_promaja/?hl=en" class="icon fb" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
        <p class="copyright"><small>Copyright © 2017 Restoran Promaja – Beograd</small></p>
    </div>
</footer>
<!-- END FOOTER -->
