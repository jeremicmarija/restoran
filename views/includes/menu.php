

<!-- BEGIN MENU SECTION -->
<section id="menu" class="section menu">

    <div class="container">
        <div id="menu-small" class="jt_row jt_row-fluid row">
            <div class="col-md-12 jt_col column_container">
                <div class="voffset100"></div>
                <div class="voffset60"></div>
            </div>
            <h2 class="section-title">Meni</h2>
            <div class="section-subtitle">
                   U našoj ponudi svakodnevno Vas očekuju raznovrsna jela<br> spremljena po tradicionalnim recepturama.
            </div>

            <div class="col-md-4 jt_col column_container small-menu">
                <h3 class="section-title menu-title">Jelovnik</h3>
                <p>Posebno izdvajamo hladna i topla predjela, pazarski roštilj, pečenje ispod sača, morsku i rečnu ribu,riižoto, paste i obrok salate.</p>
                <div class="small-resolution">
                    <a href="./full-menu.php#eat" class="button menu center">Pogledaj jelovnik</a>
                </div> 
            </div>

            <div class="col-md-4 jt_col column_container small-menu">
                <h3 class="section-title menu-title">Karta pića</h3>
                <p> Jedno od retkih mesta u Beogradu gde možete poručiti domaću kafu u fildžanu kao i druge kafe iz naše ponude. Isto tako nudimo široki asortiman bezalkholnog i alkoholnog pića.</p>
                <div class="small-resolution">
                    <a href="./full-menu.php#drink" class="button menu center">Pogledaj kartu pića</a>
                </div>
            </div>

            <div class="col-md-4 jt_col column_container small-menu">
                <h3 class="section-title menu-title">Vinska karta</h3>
                <p>Veliki izbor belih, crvenih i roze domaćih i stranih vina.Vinarije Kovačević, Vukoje, Plantaže, Zvonko Bogdan kao i vina iz Makedonije, Italije, Čilea.</p>
                <div class="small-resolution">
                    <a href="./full-menu.php#vine" class="button menu center">Pogledaj vinsku kartu</a>
                  </div>
                </div>
        </div>

        <div class="jt_row jt_row-fluid row big-resolution">
            <div class="col-md-4 jt_col column_container small-menu buttonlook">
                 <a href="./full-menu.php#eat" class="button menu center">Pogledaj jelovnik</a>
            </div>
            <div class="col-md-4 jt_col column_container small-menu buttonlook">
                 <a href="./full-menu.php#drink" class="button menu center">Pogledaj kartu pića</a>
            </div>
            <div class="col-md-4 jt_col column_container small-menu buttonlook">
                 <a href="./full-menu.php#vine" class="button menu center">Pogledaj vinsku kartu</a>
            </div>
        </div>      
    </div>
</section>
<!-- END MENU SECTION -->
