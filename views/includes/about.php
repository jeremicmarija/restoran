<!-- BEGIN ABOUT SECTION -->
<section id="about" class="section about">
    <div class="container">
        <div class="jt_row jt_row-fluid row">
            <div class="col-md-12 jt_col column_container">
                <h2 class="section-title">O Nama</h2>
            </div>
            <div class="col-md-6 col-md-offset-3 jt_col column_container">
                <div class="section-subtitle">
                    Klub-Restoran Promaja nalazi se na jednoj od najlepših i najatraktivnijih lokacija u Beogradu.
                </div>
            </div>

        </div>
        <div class="jt_row jt_row-fluid row">
            <div class="col-md-6 jt_col column_container">
                <div class="voffset10"></div>
                <div id="owl-about2" class="owl-carousel">
                    <div class="item"><img src="images/about/about1.png" alt=""></div>
                    <div class="item"><img src="images/about/about2.png" alt=""></div>
                    <div class="item"><img src="images/about/about3.png" alt=""></div>
                </div>
            </div>

            <div class="col-md-6 jt_col column_container">
                <p class="text">
                    Na Savskom pristaništu, nadomak Brankovog mosta, svojim gostima garantuje ugodan ambijent,
                    bilo za jutarnju kafu i doručak sa prijateljima, poslovni ručak sa kolegama ili romantičnu večeru u dvoje.
                    A u kasnijim večernjim satima ovde se možete opustiti i uživati u živoj svirci uz sjajnu ponudu hrane i pića.
                </p>
                <p class="text">
                    Takođe, Klub-Restoran Promaja je idealno mesto gde možete organizovati proslave poput rođendana, svadbi,krštenja,poslovnih ručkova i večera...
                    Tokom više od pola veka,koliko postojimo,ponosno stojimo iza kvaliteta naše ponude i usluge.<br>
                    Dobro došli.
                </p>
            </div>
        </div>

    </div>

</section>
<!-- END ABOUT SECTION -->