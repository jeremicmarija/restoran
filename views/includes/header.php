<div id="mask">
    <div class="loader">
        <img src="svg-loaders/tail-spin.svg" alt='loading'>
    </div>
</div>



<!-- BEGIN HEADER -->
<header id="header" role="banner">
    <div class="jt_row container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand normal logo" href="#home-slider"><p class="logo-title" id="logo-title-white">Restaurant & Club</p><div class="logo_elixir logowhite"></div></a>
            <a class="navbar-brand mini" href="#home-slider"><p class="logo-title" id="logo-title-black">Restaurant & Club</p><div class="logo_elixir dark"></div></a>
            <a class="navbar-brand mini darker" href="#home-slider"><p class="logo-title" id="logo-title-black">Restaurant & Club</p><div class="logo_elixir dark"></div></a>
        </div>

        <!-- BEGIN NAVIGATION MENU-->
        <nav class="collapse navbar-collapse navbar-right navbar-main-collapse" role="navigation">
            <ul id="nav" class="nav navbar-nav navigation">
                <!-- <li class="page-scroll menu-item"><a href="#home-slider">Pocetna</a></li> -->
                <li class="page-scroll menu-item"><a href="#about">O nama</a></li>
                <li class="page-scroll menu-item"><a href="#menu">Meni</a></li>
                <li class="page-scroll menu-item"><a href="#gallery">Galerija</a></li>
                <li class="page-scroll menu-item"><a href="#reservations">Rezervacije</a></li>
                <li class="page-scroll menu-item"><a href="#contact">Lokacija</a></li>
            </ul>
        </nav>
        <!-- EN NAVIGATION MENU -->
    </div>
</header>
<!-- END HEADER -->




<!-- BEGIN HOME SLIDER SECTION -->
<section id="home-slider">
    <div class="overlay"></div>
    <!-- SLIDER IMAGES -->
    <div id="owl-main" class="owl-carousel">
        <div class="item"><img class="img-slide img-responsive" src="images/slider/slider1.jpg" alt="Slide 011"></div>
        <div class="item"><img  class="img-slide img-responsive" src="images/slider/slider2.jpg" alt="Slide 022"></div>
        <div class="item"><img  class="img-slide img-responsive" src="images/slider/slider3.jpg" alt="Slide 033"></div>
        <div class="item"><img class="img-slide img-responsive" src="images/slider/slider4.jpg" alt="Slide 044"></div>

    </div>
    <!-- SLIDER CONTENT -->
    <!--    <div class="slide-content">-->
    <!--        <div class="voffset100"></div>-->
    <!--        <h1>Restaurant & Club Promaja</h1>-->
    <!---->
    <!--        <div class="slide-sep"></div>-->
    <!--        <div id="owl-main-text" class="owl-carousel">-->
    <!--            <div class="item">-->
    <!--                <p>Tajna dobre hrane nadomak Brankovog mosta</p>-->
    <!--            </div>-->
    <!--            <div class="item">-->
    <!--                <p>Vrhunski specijaliteti internacionalne i domaće kuhinje</p>-->
    <!--            </div>-->
    <!--            <div class="item">-->
    <!--               <p>Bar po vasem ukusu</p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!-- BOTTOM ANIMATED ARROW -->
    <a href="#about" class="page-scroll menu-item"><div class="mouse"><span></span></div></a>
</section>
<!-- END HOME SLIDER SECTION -->
