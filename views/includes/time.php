<!-- BEGIN TIMETABLE SECTION -->
<section id="timetable" class="section timetable parallax">
    <div class="container">
        <div class="jt_row jt_row-fluid row">
            <div class="col-md-12 jt_col column_container">
                <h2 id="times" class="section-title"><span class="timetable-decorator"></span><span class="opening-hours">Radno vreme</span><span class="timetable-decorator2"></span></h2>
            </div>
            <div class="col-md-12 jt_col column_container">
                <div class="section-subtitle">
                    <p>Pozovite nas da rezervišete</p>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-3 jt_col column_container">
                <div class="section-subtitle days">
                    Nedelja do Četvrtka
                </div>
                <div class="section-subtitle hours">
                    09:00 <br> 24:00
                </div>
            </div>
            <div class="col-md-3 jt_col column_container">
                <div class="section-subtitle days">
                    Petak i <br>Subota
                </div>
                <div class="section-subtitle hours">
                    09:00 <br> 02:00
                </div>
            </div>
            <div class="col-md-12 jt_col column_container">
                <div class="number">
                    <p>BROJEVI ZA REZERVACIJE: </p>
                    <p>011/262-80-96 ; 064/8-666-660</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END TIMETABLE SECTION -->

