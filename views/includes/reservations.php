<!-- BEGIN RESERVATIONS SECTION -->
<section id="reservations" class="section reservations">
    <div class="container">
        <div class="jt_row jt_row-fluid row">
            <div class="col-md-12 jt_col column_container">
                <h2 class="section-title">Rezervacije</h2>
            </div>
            <div class="col-md-6 col-md-offset-3 jt_col column_container">
                <div class="section-subtitle">
                    Brzo i jednostavno rezervišite sto u našem restoranu.<br>
                    Popunite formu i uskoro ćemo vam potvrditi željeni termin.
                </div>
            </div>
            <div class="col-md-12 jt_col column_container">
                <!--                <div class="reservations_logo"></div>-->
                <!--                <div class="voffset40"></div>-->

                <!--                <h4><span class="above">Dobrodosli</span></h4>-->
                <!--                <h3>Napravi rezervaciju</h3>-->
                <h4><span>Radno vreme</span></h4>
                <div class="voffset50"></div>
                <p class="radno-vreme">
                  <span class="group">
                    <strong>Nedelja - Četvrtak</strong><span> 09:00 - 24:00</span>
                  </span>
                    <br><br>
                    <span class="group">
                    <strong>Petak i Subota</strong><span> 09:00 - 02:00<span>
                  </span>
                </p>
                <h3 class="reservation-phone">  011/262-80-96 ; 064/8-666-660</h3>
            </div>

            <form action="/newversion/reservations.php" method="post" id="reservationform" class="contact-form">
                <div class="col-md-5 col-md-offset-1 jt_col column_container">
                    <p>Detaji rezervacije</p>
                    <input type="text" id="date" name="date" placeholder="Datum" class="text date required" >
                    <input type="text" id="time" name="time" placeholder="Vreme" class="text time required" >
                    <input type="text" id="party" name="party" placeholder="Povod" class="text party required" >
                </div>

                <div class="col-md-5 jt_col column_container">
                    <p>Kontakt informacije</p>
                    <input type="text" id="reservation_name" name="fullname" placeholder="Ime i Prezime" class="text reservation_name required" >
                    <input type="email" id="reservation_email" name="email" class="tex email required" placeholder="Email" >
                    <input type="text" id="reservation_phone" name="phone" placeholder="Telefon" class="text reservation_phone required">
                </div>

                <div class="col-md-10 col-md-offset-1 jt_col column_container">
                    <textarea id="reservation_message" name="textmsg" class="text area required" placeholder="Poruka" rows="6"></textarea>
                </div>
                <div class="col-md-4 col-md-offset-4 jt_col column_container">
                    <div id="res-status" class="formSent"></div>
                    <input type="submit" class="button menu center" id="sendReservation" value="Rezerviši" >
                </div>
            </form>

        </div>
    </div>
</section>
<!-- END RESERVATIONS SECTION-->
