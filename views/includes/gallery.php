<!-- BEGIN GALLERY SECTION -->
<section id="gallery" class="section gallery dark">
    <div id="gallery-back" class="jt_row jt_row-fluid row">
        <div class="col-md-12 jt_col column_container">
            <h2 class="section-title">Galerija</h2>
            <div class="col-md-6 col-md-offset-3 jt_col column_container">
                <div class="section-subtitle">
                    <!--                    If a picture says a thousand words, then you can imagine how long it would take to describe all our mouthwatering selections.-->
                </div>
            </div>

        </div>

        <div class="col-md-12 jt_col column_container">

            <div class="portfolio">

                <article class="entry burgers">
                    <a class="swipebox" href="images/gallerynew/promaja-galerija-1.jpg">
                        <img src="images/gallerynew/promaja-galerija-1.jpg" class="img-responsive" alt=""/>
                        <span class="magnifier"></span>
                    </a>
                </article>

                <article class="entry complements">
                    <a class="swipebox" href="images/gallerynew/promaja-galerija-4.jpg">
                        <img src="images/gallerynew/promaja-galerija-4.jpg" class="img-responsive" alt=""/>
                        <span class="magnifier"></span>
                    </a>
                </article>

                <article class="entry dessert">
                    <a class="swipebox" href="images/gallerynew/promaja-galerija-7.jpg">
                        <img src="images/gallerynew/promaja-galerija-7.jpg" class="img-responsive" alt=""/>
                        <span class="magnifier"></span>
                    </a>
                </article>

                <article class="entry burgers">
                    <a class="swipebox" href="images/gallerynew/promaja-galerija-12.png">
                        <img src="images/gallerynew/promaja-galerija-12.png" class="img-responsive" alt=""/>
                        <span class="magnifier"></span>
                    </a>
                </article>

                <article class="entry burgers">
                    <a class="swipebox" href="images/gallerynew/galerija1new.png">
                        <img src="images/gallerynew/galerija1new.png" class="img-responsive" alt=""/>
                        <span class="magnifier"></span>
                    </a>
                </article>

                <article class="entry dessert">
                    <a class="swipebox" href="images/gallerynew/galerija2new.png">
                        <img src="images/gallerynew/galerija2new.png" class="img-responsive" alt=""/>
                        <span class="magnifier"></span>
                    </a>
                </article>

                <article class="entry dessert">
                    <a class="swipebox" href="images/gallerynew/galerija3new.png">
                        <img src="images/gallerynew/galerija3new.png" class="img-responsive" alt=""/>
                        <span class="magnifier"></span>
                    </a>
                </article>

                <article class="entry dessert">
                    <a class="swipebox" href="images/gallerynew/galerija4new.png">
                        <img src="images/gallerynew/galerija4new.png" class="img-responsive" alt=""/>
                        <span class="magnifier"></span>
                    </a>
                </article>

                <article class="entry dessert">
                    <a class="swipebox" href="images/gallerynew/galerija5new.png">
                        <img src="images/gallerynew/galerija5new.png" class="img-responsive" alt=""/>
                        <span class="magnifier"></span>
                    </a>
                </article>


            </div>
        </div> <!-- End .jt_col -->
    </div> <!-- End .jt_row -->
</section>
<!-- END GALLERY SECTION -->