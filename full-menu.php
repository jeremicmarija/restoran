<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include __DIR__ . '/views/includes/head.php';
    ?>
</head>

<!-- BEGIN HEADER -->
<header id="header" role="banner">
    <div class="jt_row container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand normal logo" href="#home-slider"><p class="logo-title" id="logo-title-white">Restaurant & Club</p><div class="logo_elixir logowhite"></div></a>
            <a class="navbar-brand mini" href="/newversion/"><p class="logo-title" id="logo-title-black">Restaurant & Club<div class="logo_elixir dark"></div></a>
            <a class="navbar-brand mini darker" href="/newversion/"><p class="logo-title" id="logo-title-black">Restaurant & Club<div class="logo_elixir dark"></div></a>

        </div>

        <!-- BEGIN NAVIGATION MENU-->
        <nav class="collapse navbar-collapse navbar-right navbar-main-collapse" role="navigation">
            <ul class="nav navbar-nav navigation">
                <li class="page-scroll menu-item"><a href="/newversion/">Nazad na početnu</a></li>
            </ul>
        </nav>
        <!-- EN NAVIGATION MENU -->
    </div>
</header>
<!-- END HEADER -->

<section id="fullmenu" class="section menu">
  <div class="container">
    <div id="eat" class="row">
      <div class="col-md-12 padding--reset">
          <div class="voffset100"></div>
          <h1  class="title first fullmenu">Jelovnik</h1>
          <div class="voffset10"></div>
          <img class="center" src="images/jelovnik.jpg" alt="Steaks">
      </div>
    </div>
    <!-- Dorucak -->
    <div class="row menu__row jt_row">
      <h3 class="menu__heading">
        Doručak / Breakfast
        <br>
        09:00 - 12:30
      </h3>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Omlet</span>
            <span class="list-item__description">(sir, šunka, slanina)</span>
            <span class="list-item__title">Omlette</span>
            <span class="list-item__description">(with cheese,ham,bacon)</span>
            <span class="list-item__price">260,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Sendvič Promaja</span>
            <span class="list-item__description">(prženice, šunka, kačkavalj, pavlaka)</span>
            <span class="list-item__title">Sandwich Promaja</span>
            <span class="list-item__description">(french toast, ham, cheese, sour cream)</span>
            <span class="list-item__price">260,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Engleski doručak</span>
            <span class="list-item__description">(jaja, teleća kobasica, slanina, sir)</span>
            <span class="list-item__title">English breakfast</span>
            <span class="list-item__description">(eggs, sausage, bacon, cheese)</span>
            <span class="list-item__price">350,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Jogurt 0,2</span>
            <span class="list-item__title">Yogurt 0,2</span>
            <span class="list-item__price">80,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Mleko 0,2</span>
            <span class="list-item__title">Milk 0,2</span>
            <span class="list-item__price">80,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Dorucak -->
    <!-- Predjela -->
    <div class="row menu__row jt_row">
      <!-- Hladna Predjela -->
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Hladna predjela / Cold Hors D'oeuvres</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Njeguški pršut</span>
            <span class="list-item__title">"Njegush" smoked ham</span>
            <span class="list-item__description">(100g)</span>
            <span class="list-item__price">650,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Goveđi pršut</span>
            <span class="list-item__title">Beef smoked ham</span>
            <span class="list-item__description">(100g)</span>
            <span class="list-item__price">650,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Tatar biftek</span>
            <span class="list-item__title">Steak Tatar</span>
            <span class="list-item__description">(175g)</span>
            <span class="list-item__price">1050,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Sjenički sir</span>
            <span class="list-item__title">Sjenica cheese</span>
            <span class="list-item__description">(100g)</span>
            <span class="list-item__price">260,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Ovčiji / Kozji sir</span>
            <span class="list-item__title">Sheep/Goat cheese</span>
            <span class="list-item__description">(100g)</span>
            <span class="list-item__price">300,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Kajmak</span>
            <span class="list-item__title">Cream-cheese</span>
            <span class="list-item__description">(100g)</span>
            <span class="list-item__price">350,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Selekcija sireva</span>
            <span class="list-item__title">Selection of cheeses</span>
            <span class="list-item__description">(250g)</span>
            <span class="list-item__price">750,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Brusketi</span>
            <span class="list-item__description">(paradajz, mocarela, masline, masl.ulje, b.luk)</span>
            <span class="list-item__title">Bruschette</span>
            <span class="list-item__description">(tomato, mozzarella, olives, olive oil, garlic)</span>
            <span class="list-item__price">750,00</span>
          </li>
        </ul>
      </div>
      <!-- (x) Hladna predjela -->
      <!-- Topla Predjela -->
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Topla predjela / Warm Hors D'oeuvres</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Pohovani kačkavalj</span>
            <span class="list-item__title">Fried cheese</span>
            <span class="list-item__price">450,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Punjene pečurke</span>
            <span class="list-item__description">(pečurke, šunka, kačkavalj, origano)</span>
            <span class="list-item__title">Stuffed mushrooms</span>
            <span class="list-item__description">(mushrooms, ham, cheese, oregano)</span>
            <span class="list-item__price">450,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Grilovano povrće</span>
            <span class="list-item__description">(tikvice, patlidzan, pečurke, paradajz)</span>
            <span class="list-item__title">Roasted vegetables</span>
            <span class="list-item__description">(zucchini, eggplant, mushrooms, tomato)</span>
            <span class="list-item__price">320,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Predjela -->
    <!-- Corbe i salate -->
    <div class="row menu__row jt_row">
      <!-- Corbe -->
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Čorbe, supe / Soups</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Riblja čorba</span>
            <span class="list-item__title">Fish soup</span>
            <span class="list-item__price">250,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Teleća čorba</span>
            <span class="list-item__title">Veal soup</span>
            <span class="list-item__price">250,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Goveđa supa</span>
            <span class="list-item__title">Beef soup</span>
            <span class="list-item__price">250,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Potaž dana</span>
            <span class="list-item__title">Daily pottage</span>
            <span class="list-item__price">250,00</span>
          </li>
        </ul>
      </div>
      <!-- (x) Corbe -->
      <!-- Salate -->
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Obrok salate / Main Dish Salads</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Beef salata</span>
            <span class="list-item__description">(ind.orah, susam, paradajz, miks zelenih salata, biftek, dresing, parmezan)</span>
            <span class="list-item__title">Beef salad</span>
            <span class="list-item__description">(ind nuts, sesame, lettuce, beefsteak, dressing, parmesan)</span>
            <span class="list-item__price">720,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Cezar salata</span>
            <span class="list-item__description">(miks zelenih salata, piletina, slanina, dresing, parmezan, paradajz, tost)</span>
            <span class="list-item__title">Ceasar salad</span>
            <span class="list-item__description">(lettuce, chicken, bacon, dressing, parmesan, tomato, toast)</span>
            <span class="list-item__price">620,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Kapreze salata</span>
            <span class="list-item__description">(paradajz, mocarela, maslinovo ulje, bosiljak)</span>
            <span class="list-item__title">Caprese salad</span>
            <span class="list-item__description">(tomato, mozzarela, olive oil, basil)</span>
            <span class="list-item__price">480,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Grčka salata</span>
            <span class="list-item__description">(paradajz, paprika, krastavac, sir, masline, crveni luk, masl.ulje)</span>
            <span class="list-item__title">Greek salad</span>
            <span class="list-item__description">(tomato, paprika, cucumber, cheese, olives, onion, olive oil)</span>
            <span class="list-item__price">390,00</span>
          </li>
        </ul>
      </div>
      <!-- (x) Salate -->
    </div>
    <!-- (x) Corbe i salate-->
    <!-- Paste i rizoto-->
    <div class="row menu__row jt_row">
      <!-- Paste -->
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Paste / Pasta</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Arabijata</span>
            <span class="list-item__description">(paradajz, bosiljak, čili)</span>
            <span class="list-item__title">Arabiata</span>
            <span class="list-item__description">(tomato, basil, chili)</span>
            <span class="list-item__price">550,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Vegetarijana</span>
            <span class="list-item__description">(miks povrća)</span>
            <span class="list-item__title">Vegetariana</span>
            <span class="list-item__description">(vegetable mix)</span>
            <span class="list-item__price">550,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pesto Đenoveze</span>
            <span class="list-item__description">(pesto sos, belo vino, neutralna pavlaka)</span>
            <span class="list-item__title">Pesto Genovese</span>
            <span class="list-item__description">(pesto sauce, white wine, cream)</span>
            <span class="list-item__price">560,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Plodovi mora</span>
            <span class="list-item__description">(pl.mora, crni luk, beli luk, b.vino, pelat)</span>
            <span class="list-item__title">Frutti di mare</span>
            <span class="list-item__description">(seafood, onion, garlic, white wine, tomato)</span>
            <span class="list-item__price">620,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Karbonara</span>
            <span class="list-item__description">(slanina, jaje, parmezan, neut.pavlaka, b.luk)</span>
            <span class="list-item__title">Carbonara</span>
            <span class="list-item__description">(bacon, egg, parmesan, cream, garlic)</span>
            <span class="list-item__price">580,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Četiri vrste sira</span>
            <span class="list-item__title">Quatro formagio</span>
            <span class="list-item__price">620,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pasta sa pršutom</span>
            <span class="list-item__description">(njeguška pršuta, belo vino, neut.pavlaka, vorčester sos, parmezan)</span>
            <span class="list-item__title">Pasta Prosciutto</span>
            <span class="list-item__description">(smoked ham, white wine, cream, worcester,sauce, parmesan)</span>
            <span class="list-item__price">620,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Primavera</span>
            <span class="list-item__description">(čeri paradajz, rukola, masl.ulje, mocarela, beli luk, belo vino, verde sos, tikvice)</span>
            <span class="list-item__title">Primavera</span>
            <span class="list-item__description">(cherry tomato, rucola, mozzarella, garlic, white wine, verde sauce, zucchini)</span>
            <span class="list-item__price">600,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pikante "pene" sa biftekom</span>
            <span class="list-item__description">(biftek, pene, tomatino, cili sos, mocarela, rukola)</span>
            <span class="list-item__title">Chili "pene" with beef</span>
            <span class="list-item__description">(beef, pene,tomatino, chili, rukola, mocarela)</span>
            <span class="list-item__price">780,00</span>
          </li>
        </ul>
      </div>
      <!-- (x) Paste -->
      <!-- Rizoto -->
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Rižoto / Risotto</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Rižoto sa povrćem</span>
            <span class="list-item__description">(miks povrća, soja sos, pelat, b.vino)</span>
            <span class="list-item__title">Risotto with vegetables</span>
            <span class="list-item__description">(vegetable mix, soya sauce, tomato sauce, w.wine)</span>
            <span class="list-item__price">550,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Rižoto sa piletinom i karijem</span>
            <span class="list-item__description">(piletina, kari, pelat, b.vino)</span>
            <span class="list-item__title">Risotto with chicken and curry</span>
            <span class="list-item__description">(chicken, curry, tomato sauce, w.wine)</span>
            <span class="list-item__price">620,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Plodovi mora</span>
            <span class="list-item__description">(pl.mora, crni luk, beli luk, b.vino, pelat)</span>
            <span class="list-item__title">Frutti di mare</span>
            <span class="list-item__description">(seafood, onion, garlic, white wine, tomato)</span>
            <span class="list-item__price">650,00</span>
          </li>
        </ul>
      </div>
      <!-- (x) Rizoto -->
    </div>
    <!-- (x) Paste i rizoto-->
    <!-- Jela po porudzbini -->
    <div class="row menu__row jt_row">
      <h3 class="menu__heading">Jela po porudžbini / A la carte</h3>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Pileća saltimboka</span>
            <span class="list-item__description">(piletina, njeguška pršuta, belo vino, pasta)</span>
            <span class="list-item__title">Chicken saltimboca</span>
            <span class="list-item__description">(chicken, njegush smoked ham, w.wine, pasta)</span>
            <span class="list-item__price">720,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pileći medaljoni u sosu od pečuraka</span>
            <span class="list-item__description">(pileći file, sos od pečuraka<span class="red">*</span> )</span>
            <span class="list-item__title">Chicken medallions in mushroom sauce</span>
            <span class="list-item__description">(ch.breasts, mushroom sauce<span class="red">*</span> )</span>
            <span class="list-item__price">720,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Santa Venerina</span>
            <span class="list-item__description">(piletina, mocarela, luk, pelat)</span>
            <span class="list-item__title">Santa Venerina</span>
            <span class="list-item__description">(chicken, mozzarella, onion, tomato)</span>
            <span class="list-item__price">720,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Ćuretina gorgonzola</span>
            <span class="list-item__description">(ćureći file, gorgonzola, neutr. pavlaka)</span>
            <span class="list-item__title">Turkey gorgonzola</span>
            <span class="list-item__description">(turkez breasts, gorgonzola, cream)</span>
            <span class="list-item__price">850,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Ćureći medaljoni u sosu od pečuraka</span>
            <span class="list-item__description">(crni luk, sos od pečuraka<span class="red">*</span> )</span>
            <span class="list-item__title">Turkey medallions with mushroom sauce</span>
            <span class="list-item__description">(onion, mushroom sauce<span class="red">*</span> )</span>
            <span class="list-item__price">850,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Teleća mućkalica</span>
            <span class="list-item__description">(tel.meso, crni luk, ljuta i slatka paprika, paradajz)</span>
            <span class="list-item__title">Veal "Muckalica"</span>
            <span class="list-item__description">(veal, onion, hot and sweet papper, tomato)</span>
            <span class="list-item__description">(250g)</span>
            <span class="list-item__price">720,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Rolovana teletina</span>
            <span class="list-item__description">(porcija sa prilogom)</span>
            <span class="list-item__title">Rolled veal</span>
            <span class="list-item__description">(serving side dish)</span>
            <span class="list-item__description">(300g)</span>
            <span class="list-item__price">1080,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Jela po porudzbini -->
    <!-- Stekovi -->
    <div class="row menu__row jt_row">
      <h3 class="menu__heading">Stekovi / Steak</h3>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Biftek Monte Karlo</span>
            <span class="list-item__description">(biftek, jaje)</span>
            <span class="list-item__title">Beefsteak Monte Carlo</span>
            <span class="list-item__description">(beefsteak, egg)</span>
            <span class="list-item__description">(250g)</span>
            <span class="list-item__price">1250,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Biftek pepper sauce</span>
            <span class="list-item__description">(biftek, zeleni biber, neutralna pavlaka)</span>
            <span class="list-item__title">Beefsteak pepper sauce</span>
            <span class="list-item__description">(beefsteak, green pepper, cream)</span>
            <span class="list-item__description">(250g)</span>
            <span class="list-item__price">1250,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Biftek u sosu od pečuraka</span>
            <span class="list-item__description">(biftek, sos od pečuraka<span class="red">*</span> )</span>
            <span class="list-item__title">Beefsteak in mushroom sauce</span>
            <span class="list-item__description">(beefsteak, mushroom sauce<span class="red">*</span> )</span>
            <span class="list-item__description">(250g)</span>
            <span class="list-item__price">1250,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Biftek Promaja</span>
            <span class="list-item__description">(biftek, Promaja sos<span class="red">*</span> )</span>
            <span class="list-item__title">Beefsteak Promaja</span>
            <span class="list-item__description">(beefsteak, Promaja sauce<span class="red">*</span> )</span>
            <span class="list-item__description">(250g)</span>
            <span class="list-item__price">1300,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Stekovi -->
    <!-- Sosevi -->
    <div class="row menu__row jt_row">
      <h3 class="menu__heading">Sosevi / Sauses</h3>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item list-item--full">
            <span class="list-item__title"><span class="red">*</span> Biftek Promaja</span>
            <span class="list-item__description">šampinjoni, tomatino, goveđa kocka, crveno vino, ljuta paprika, zeleni biber, beli luk, masl. ulje</span>
            <span class="list-item__title"><span class="red">*</span> Promaja sauce</span>
            <span class="list-item__description">champignons, tomato sauce, beef cube, red wine, chili, green pepper, garlic, olive oil</span>
          </li>
          <li class="menu-list__item list-item--full">
            <span class="list-item__title"><span class="red">*</span> Pepper sauce</span>
            <span class="list-item__description">neutralna pavlaka, zeleni biber, belo vino</span>
            <span class="list-item__title"><span class="red">*</span> Pepper sauce</span>
            <span class="list-item__description">cream, green papper, white wine</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item list-item--full">
            <span class="list-item__title"><span class="red">*</span> Sos od pečuraka</span>
            <span class="list-item__description">šitaki, reishi pečurke, šampinjoni, neutralna pavlaka, vorčester sos, beli luk, belo vino</span>
            <span class="list-item__title"><span class="red">*</span> Mushroom sauce</span>
            <span class="list-item__description">shiitake, reishi, champignons, cream,worcester sauce, garlic, olive oil</span>
          </li>
          <li class="menu-list__item list-item--full">
            <span class="list-item__title"><span class="red">*</span> Mirođija sos</span>
            <span class="list-item__description">neutralna pavlaka, mirođija, belo vino, limunov sok, začin</span>
            <span class="list-item__title"><span class="red">*</span> Dill sauce</span>
            <span class="list-item__description">cream, dill, white wine, lemon juice, spice</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Sosevi -->
    <!-- Riba i Rostilj -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Riba / Fish</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Šaran</span>
            <span class="list-item__description">(mediteranski prilog)</span>
            <span class="list-item__title">Carp</span>
            <span class="list-item__description">(mediterranean side dish)</span>
            <span class="list-item__price">1600,00 / kg</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Som</span>
            <span class="list-item__description">(mediteranski prilog)</span>
            <span class="list-item__title">Catfish</span>
            <span class="list-item__description">(mediterranean side dish)</span>
            <span class="list-item__price">1800,00 / kg</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Smuđ</span>
            <span class="list-item__description">(mediteranski prilog)</span>
            <span class="list-item__title">Perch</span>
            <span class="list-item__description">(mediterranean side dish)</span>
            <span class="list-item__price">1950,00 / kg</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Sveža pastrmka</span>
            <span class="list-item__description">(mediteranski prilog)</span>
            <span class="list-item__title">Fresh trout</span>
            <span class="list-item__description">(mediterranean side dish)</span>
            <span class="list-item__price">1850,00 / kg</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Brancin</span>
            <span class="list-item__description">(mediteranski prilog)</span>
            <span class="list-item__title">Bass</span>
            <span class="list-item__description">(mediterranean side dish)</span>
            <span class="list-item__price">3250,00 / kg</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Losos stek u sosu</span>
            <span class="list-item__description">(mediteranski prilog)</span>
            <span class="list-item__title">Salmon steak with sauce</span>
            <span class="list-item__description">(mediterranean side dish)</span>
            <span class="list-item__description">(250g)</span>
            <span class="list-item__price">1020,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Lignje po želji</span>
            <span class="list-item__description">(frigane, pohovane, na žaru)</span>
            <span class="list-item__title">Squids</span>
            <span class="list-item__description">(fried, deep fried, roasted)</span>
            <span class="list-item__description">(250g)</span>
            <span class="list-item__price">860,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Roštilj / Barbecue</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Pazarski ćevapi</span>
            <span class="list-item__title">"Cevapcici" from Pazar</span>
            <span class="list-item__description">(300g)</span>
            <span class="list-item__price">680,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pazarska pljeskavica</span>
            <span class="list-item__title">Burger from Pazar</span>
            <span class="list-item__description">(300g)</span>
            <span class="list-item__price">680,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Gurmanska pljeskavica</span>
            <span class="list-item__description">(slanina, kačkavalj, tucana paprika)</span>
            <span class="list-item__title">Gourmet burger</span>
            <span class="list-item__description">(bacon, cheese, hot paprika)</span>
            <span class="list-item__description">(300g)</span>
            <span class="list-item__price">780,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Mešano meso</span>
            <span class="list-item__description">(kobasica, ćevap, pljeskavica, pileće belo, krmenadla, slanina)</span>
            <span class="list-item__title">Mixed grilled meat</span>
            <span class="list-item__description">(sausage, cevapcici, burger, checken breasts, rib'eze steak, bacon)</span>
            <span class="list-item__description">(450g)</span>
            <span class="list-item__price">980,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Teleća kobasica</span>
            <span class="list-item__title">Veal sausage</span>
            <span class="list-item__description">(300g)</span>
            <span class="list-item__price">780,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Rolovana piletina na žaru</span>
            <span class="list-item__description">(pileće belo, kačkavalj, pančeta)</span>
            <span class="list-item__title">Roasted rolled chicken</span>
            <span class="list-item__description">(chicken breast, cheese, pancetta)</span>
            <span class="list-item__price">780,00 / kg</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Piletina na žaru</span>
            <span class="list-item__title">Roasted Chicken</span>
            <span class="list-item__description">(250g)</span>
            <span class="list-item__price">680,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Kotlet teleći</span>
            <span class="list-item__description">(porcija sa prilogom)</span>
            <span class="list-item__title">Veal cutlet</span>
            <span class="list-item__description">(serving side dish)</span>
            <span class="list-item__description">(300g)</span>
            <span class="list-item__price">1080,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Riba i Rostilj -->
    <!-- Sac i kuvano jelo dana -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Sač / Bell</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Jagnjetina / Jaretina ispod sača</span>
            <span class="list-item__description">(sa prilogom)</span>
            <span class="list-item__title">Lamb / Baby goat under the bell</span>
            <span class="list-item__description">(side dish)</span>
            <span class="list-item__price">2000,00 / kg</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Teletina ispod sača</span>
            <span class="list-item__description">(sa prilogom)</span>
            <span class="list-item__title">Veal under the bell</span>
            <span class="list-item__description">(side dish)</span>
            <span class="list-item__price">2400,00 / kg</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Piletina ispod sača</span>
            <span class="list-item__description">(sa prilogom)</span>
            <span class="list-item__title">Chicken under the bell</span>
            <span class="list-item__description">(side dish)</span>
            <span class="list-item__price">1200,00 / kg</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Kuvano jelo dana <br>Meal of the day</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Jelo + salata + hleb</span>
            <span class="list-item__title">Mealdish + salad + bread</span>
            <span class="list-item__price">480,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Sac i kuvano jelo dana -->
    <!-- Salate -->
    <div class="row menu__row jt_row">
      <h3 class="menu__heading">Salate / Salads</h3>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Srpska</span>
            <span class="list-item__description">(paradajz, krastavac, paprika, crni luk, ljuta papričica)</span>
            <span class="list-item__title">Serbian salad</span>
            <span class="list-item__description">(tomato, cucumber, paprika, onion, hot paprika)</span>
            <span class="list-item__price">200,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Šopska salata</span>
            <span class="list-item__description">(paradajz, krastavac, paprika, crni luk, feta sir)</span>
            <span class="list-item__title">Shopska salad</span>
            <span class="list-item__description">(tomato, cucumber, paprika, onion, feta cheese)</span>
            <span class="list-item__price">220,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Paradajz salata</span>
            <span class="list-item__title">Tomato salad</span>
            <span class="list-item__price">200,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Zelena salata</span>
            <span class="list-item__title">Green salad</span>
            <span class="list-item__price">220,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Kupus salata</span>
            <span class="list-item__title">Cabbage salad</span>
            <span class="list-item__price">150,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Bašta salata</span>
            <span class="list-item__description">(paradajz, krastavac, paprika, crni luk, kupus, zel. salata)</span>
            <span class="list-item__title">Garden salad</span>
            <span class="list-item__description">(tomato, cucumber, paprika, onion, cabbage, lettuce)</span>
            <span class="list-item__price">300,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Rukola salata</span>
            <span class="list-item__description">(rukola, čeri paradajz, balzamiko, m.ulje)</span>
            <span class="list-item__title">Rucola salad</span>
            <span class="list-item__description">(rucola, cherry tomato, balsamic, olive oil)</span>
            <span class="list-item__price">300,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Vitaminska salata</span>
            <span class="list-item__description">(z.salata, paradajz, šargarepa, celer, jabuka)</span>
            <span class="list-item__title">Vitamin salad</span>
            <span class="list-item__description">(lettuce, tomato, carrot, celery, apple, dressing)</span>
            <span class="list-item__price">250,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Krastavac salata</span>
            <span class="list-item__title">Cucumber salad</span>
            <span class="list-item__price">200,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Masline</span>
            <span class="list-item__title">Olives</span>
            <span class="list-item__price">150,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Ajvar</span>
            <span class="list-item__title">Ajvar</span>
            <span class="list-item__description">(Serbian roasted red papper sauce)</span>
            <span class="list-item__price">250,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Ljuta papričica</span>
            <span class="list-item__title">Hot pepper</span>
            <span class="list-item__price">50,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Salate  -->
    <!-- Dezerti i dodaci -->
    <div class="row menu__row jt_row">
      <!-- Dezerti -->
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Dezerti / Desserts</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Cheesecake</span>
            <span class="list-item__price">350,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Čokoladna bomba</span>
            <span class="list-item__title">Chocolate bomb</span>
            <span class="list-item__price">370,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Banana split</span>
            <span class="list-item__price">320,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Tulumba</span>
            <span class="list-item__price">220,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Baklava</span>
            <span class="list-item__price">220,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Palačinke po želji</span>
            <span class="list-item__description">(eurokrem, plazma, med, marmelada)</span>
            <span class="list-item__title">Pancakes on request</span>
            <span class="list-item__description">(chocolate, cookies, honey, jam)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Voćni tanjir</span>
            <span class="list-item__title">Fruit plate</span>
            <span class="list-item__price">350,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Sladoled (kugla)</span>
            <span class="list-item__title">Ice cream (coop)</span>
            <span class="list-item__price">75,00</span>
          </li>
        </ul>
      </div>
      <!-- Dodaci -->
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Dodaci jelima / Side dishes</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Mediteranska garnitura</span>
            <span class="list-item__description">(krompir, blitva, belo vino, paradajz, beli luk)</span>
            <span class="list-item__title">Mediterranean side dish</span>
            <span class="list-item__description">(potato, mangel, white wine, tomato, garlic)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Grilovano povrće</span>
            <span class="list-item__title">Grilled vegetables</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pomfrit</span>
            <span class="list-item__title">French fries</span>
            <span class="list-item__price">120,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pirinač</span>
            <span class="list-item__title">Rise</span>
            <span class="list-item__price">120,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Somun</span>
            <span class="list-item__title">Bread</span>
            <span class="list-item__price">50,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Dezerti i dodaci -->

    <!-- Karta pica -->
    <div id="drink" class="jt_row jt_row-fluid row row-full-menu">
      <h1 class="title first fullmenu">Karta pića</h1>
      <div class="voffset10"></div>
      <img class="center" src="images/coffee.png" alt="Drink card">
    </div>
    <!-- Topli i hladni napici -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Topli napici / Hot drinks</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Domaća kafa</span>
            <span class="list-item__title">Domestic coffee</span>
            <span class="list-item__price">135,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Espresso kafa</span>
            <span class="list-item__price">135,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Espresso sa mlekom</span>
            <span class="list-item__title">Espresso with milk</span>
            <span class="list-item__price">150,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Espresso sa šlagom</span>
            <span class="list-item__title">Espresso with whipped cream</span>
            <span class="list-item__price">160,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Capuccino</span>
            <span class="list-item__price">160,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Café Late</span>
            <span class="list-item__price">170,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Nescafe</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Nescafe sa šlagom</span>
            <span class="list-item__title">Nescafé with whipped cream</span>
            <span class="list-item__price">210,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Topla čokolada</span>
            <span class="list-item__title">Hot chocolate</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Topla čokolada sa šlagom</span>
            <span class="list-item__title">Hot chocolate with whipped cream</span>
            <span class="list-item__price">210,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Čaj</span>
            <span class="list-item__title">Tea</span>
            <span class="list-item__price">140,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Kuvano vino</span>
            <span class="list-item__title">Mulled wine</span>
            <span class="list-item__price">250,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Hladni napici / Cold drinks</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Ice coffee</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">230,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Plazma shake</span>
            <span class="list-item__description">(0,3)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Smoothie (sezonski)</span>
            <span class="list-item__title">Smoothie (season)</span>
            <span class="list-item__description">(0,3)</span>
            <span class="list-item__price">320,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Topli i hladni napici -->
    <!-- Bezalkoholna pica i piva -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Bezalkoholna pića / Non-alcoholic beverages</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Negazirana Voda</span>
            <span class="list-item__title">Still water</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">145,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Negazirana Voda</span>
            <span class="list-item__title">Still water</span>
            <span class="list-item__description">(0,75)</span>
            <span class="list-item__price">265,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Gazirana Voda</span>
            <span class="list-item__title">Mineral water</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">145,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Gazirana Voda</span>
            <span class="list-item__title">Mineral water</span>
            <span class="list-item__description">(0,75)</span>
            <span class="list-item__price">265,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Coca-Cola</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Fanta</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Sprite</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Schweppes</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Cocta</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Negazirani sokovi</span>
            <span class="list-item__title">Fruit juice</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">210,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Ice Tea</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">230,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Orangina</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">260,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Limunada</span>
            <span class="list-item__title">Lemonade</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">190,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Ceđena pomorandža</span>
            <span class="list-item__title">Orange squash</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">250,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Guarana</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">220,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Red Bull</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">350,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Pivo / Bear</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Zaječarsko</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Zaječarsko tamno</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">225,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Krušovice</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">215,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Amstel</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">205,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Heineken</span>
            <span class="list-item__description">(0,25)</span>
            <span class="list-item__price">215,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Stella Artois</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">290,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Hoegaarden</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">330,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Leffe Blonde</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">330,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Leffe Brune</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">330,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Corona</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">365,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Zaječarsko Točeno</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">150,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Zaječarsko Točeno</span>
            <span class="list-item__description">(0,5)</span>
            <span class="list-item__price">230,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Zaječarsko Tamno Točeno</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">165,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Zaječarsko Tamno Točeno</span>
            <span class="list-item__description">(0,5)</span>
            <span class="list-item__price">245,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Krušovice Točeno</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">155,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Krušovice Točeno</span>
            <span class="list-item__description">(0,5)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Heineken Točeno</span>
            <span class="list-item__description">(0,33)</span>
            <span class="list-item__price">175,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Heineken Točeno</span>
            <span class="list-item__description">(0,5)</span>
            <span class="list-item__price">255,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Bezalkoholna pica i piva -->
    <!-- Rakija i Martini -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Rakija / Brandy</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Šljivovica</span>
            <span class="list-item__title">Brandy plum</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Jabukovača</span>
            <span class="list-item__title">Brandy apple</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">160,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Kajsijevača</span>
            <span class="list-item__title">Brandy apricot</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Viljamovka</span>
            <span class="list-item__title">Brandy pear</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Dunjevača</span>
            <span class="list-item__title">Brandy quince</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">190,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Divlja Kruška</span>
            <span class="list-item__title">Brandy wild pear</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">190,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Medovača</span>
            <span class="list-item__title">Brandy honey</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">190,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Martini</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Martini Bianco</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">220,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Martini Extra Dry</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">220,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Martini Rossato</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">220,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Rakija i martini -->
    <!-- Konjak  -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Konjak / Cognac</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Remy Martin</span>
            <span class="list-item__title">Courvoiser VS</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">410,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Remy Martin VSOP</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">520,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Remy Martin XO</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">1300,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Brandy / Brandy</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Metaxa 5*</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Vinjak</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">180,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Konjak -->
    <!-- Rum i viski -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Rum</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Bacardi Superior</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Bacardi Black</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Domaći rum</span>
            <span class="list-item__title">Domestic rum</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">140,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Viski / Whiskey</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Jameson</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">290,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Famouse Grouse</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Jim Beam</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Dewars White Label</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Johnnie Walker red</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Johnnie Walker black</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">380,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Ballantines Finest</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Ballantines 12YO</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">380,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Jack Daniels</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">310,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Chivas 12YO</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">380,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Rum i Viski -->
    <!-- Gin i vodka -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Gin</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Bombay Saphire</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">290,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Beefeater</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Vodka</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Jelzin</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">190,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Absolut</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Grey Goose</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Gin i Vodka -->
    <!-- Likeri i tekile -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Liker / Liqueur</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Gorki List</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Campari</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Jegermaister</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Bailey's</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Cointreau</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">290,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Galliano</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Drambuie</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Aperol Spritz </span>
            <span class="list-item__description">(0,05)</span>
            <span class="list-item__price">350,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Tekila / Tequila</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Agavita</span>
            <span class="list-item__price">220,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Jose Cuervo Silver</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">240,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Likeri i tekile -->
    <!-- Kokteli i cideri -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Kokteli / Cocktails</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Cosmopolitan</span>
            <span class="list-item__description">(0,15)</span>
            <span class="list-item__price">395,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Margarita</span>
            <span class="list-item__description">(0,08)</span>
            <span class="list-item__price">385,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Blue Lagoon</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">365,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Sex on the Beach</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">395,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pina Colada</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">355,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Cuba Libre</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">355,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Long Island Iced Tea</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">490,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Tequila Sunrise</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">355,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Mai Tai</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">475,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Mojito (sezonski)</span>
            <span class="list-item__title">Mojito (season)</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">475,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Cider</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Somersby</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">310,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Carling</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">310,00</span>
          </li>
        </ul>
        <h3 class="menu__heading">Dodaci / More</h3>
        <ul class="menu__list">
          <li class="menu-list__item">
            <span class="list-item__title">Sladoled Kugla</span>
            <span class="list-item__title">Ice cream ball</span>
            <span class="list-item__price">75,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Šlag</span>
            <span class="list-item__title">Whipped cream</span>
            <span class="list-item__price">25,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Mleko</span>
            <span class="list-item__title">Milk</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">20,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Mleko</span>
            <span class="list-item__title">Milk</span>
            <span class="list-item__description">(0,2)</span>
            <span class="list-item__price">80,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Sojino mleko</span>
            <span class="list-item__title">Soybean milk</span>
            <span class="list-item__description">(0,03)</span>
            <span class="list-item__price">40,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Kokteli i cideri -->
    <!-- Karta pica -->
    <div id="vine" class="jt_row jt_row-fluid row row-full-menu">
      <h1  class="title first fullmenu">Vinska karta</h1>
      <div class="voffset10"></div>
      <img class="center" src="images/vino-menu.png" alt="Vine card">
    </div>
    <!-- Bela vina -->
    <div class="row menu__row jt_row">
      <h3 class="menu__heading">Bela vina / White vine</h3>
      <div class="col-md-6 menu-items__wrapper">
        <div class="quantity__heading"><span class="red">0,75l / 0,125l</span></div>
        <ul class="menu__list--vine">
          <li class="menu-list__item">
            <span class="list-item__title">Domaće točeno belo vino</span>
            <span class="list-item__title">Domestic white vine</span>
            <span class="list-item__price">- / 180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Chardonnay, vin.Plantaže</span>
            <span class="list-item__description">(Chardonnay 100%)</span>
            <span class="list-item__price">1.400,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pro Anima, vin. Plantaže</span>
            <span class="list-item__description">(Pinot Blanc 100%)</span>
            <span class="list-item__price">2.400,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Luca, vin. Plantaže</span>
            <span class="list-item__description">(Pinot Blanc 30%, Sauvignon Blanc 30%, Chardonnay 40%)</span>
            <span class="list-item__price">2.800,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Chardonnay Los Pagos Chile</span>
            <span class="list-item__description">(Chardonnay 100%)</span>
            <span class="list-item__price">1.400,00 / 240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Pinot Grigio Italija</span>
            <span class="list-item__description">(Pinot Blanc 100%)</span>
            <span class="list-item__price">1.400,00 / 240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Cavanza Chardonnay, Cile</span>
            <span class="list-item__description">(Chardonnay 100%)</span>
            <span class="list-item__price">1.400,00 / 240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Cavanza Sauvignon Blanc, Cile</span>
            <span class="list-item__description">(Sauvignon Blanc 100%)</span>
            <span class="list-item__price">1.400,00 / 240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Duša Dunava, vin. Ključ</span>
            <span class="list-item__description">(Italijanski Rizling 100%)</span>
            <span class="list-item__price">1.800,00 / 300,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <div class="quantity__heading m--hide"><span class="red">0,75l / 0,125l</span></div>
        <ul class="menu__list--vine">
          <li class="menu-list__item">
            <span class="list-item__title">8 Tamburaša, vin. Zvonko Bogdan</span>
            <span class="list-item__description">(Sauvignon Blanc 50%, Chardonnay 20%, Pinot Blanc 20%, Muscat 5%)</span>
            <span class="list-item__price">2.100,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Chardonnay vin. Kovačević</span>
            <span class="list-item__description">(Chardonnay 100%)</span>
            <span class="list-item__price">2.400,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Sauvignon vin. Kovačević</span>
            <span class="list-item__description">(Sauvignon 100%)</span>
            <span class="list-item__price">2.400,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Trijumf, vin. Aleksandrović</span>
            <span class="list-item__description">(Chardonnay 100%)</span>
            <span class="list-item__price">2.400,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Traminac vin. Janko</span>
            <span class="list-item__description">(Traminac 100%)</span>
            <span class="list-item__price">2.600,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Primavera vin. Vukoje</span>
            <span class="list-item__description">(Žilavka 70%, Chardonnay 25%, Bena 5%)</span>
            <span class="list-item__price">1.800,00 / 300,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Chardonnay vin. Vukoje</span>
            <span class="list-item__description">(Chardonnay 50%, Malvasia Dubrovacka 50%)</span>
            <span class="list-item__price">2.800,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Žilavka vin. Vukoje</span>
            <span class="list-item__description">(Žilavka 95%, Bena 5%)</span>
            <span class="list-item__price">2.800,00 / -</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Bela vina -->
    <!-- Crvena vina -->
    <div class="row menu__row jt_row">
      <h3 class="menu__heading">Crvena vina / Red vine</h3>
      <div class="col-md-6 menu-items__wrapper">
        <div class="quantity__heading"><span class="red">0,75l / 0,125l</span></div>
        <ul class="menu__list--vine">
          <li class="menu-list__item">
            <span class="list-item__title">Domaće točeno crveno vino</span>
            <span class="list-item__title">Domestic red vine</span>
            <span class="list-item__price">- / 180,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Vranac Pro-Corde vin. Plantaže</span>
            <span class="list-item__description">(Vranac 100%)</span>
            <span class="list-item__price">1.600,00 / 270,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Vladika vin. Plantaže</span>
            <span class="list-item__description">(Vranac 80%. Cabarnet Sauvignon 10%, Merlot 10%)</span>
            <span class="list-item__price">2.800,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Cavanza Cab.Sauvignon, Cile</span>
            <span class="list-item__description">(Cabarnet Sauvignon 100%)</span>
            <span class="list-item__price">1.400,00 / 240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Cab. Sauvignon, Los Pagos Chile</span>
            <span class="list-item__description">(Cabarnet Sauvignon 100%)</span>
            <span class="list-item__price">1.600,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Cab. Sauvignon vin. Radovanović</span>
            <span class="list-item__description">(Cabarnet Sauvignon 100%)</span>
            <span class="list-item__price">2.400,00 / -</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <div class="quantity__heading m--hide"><span class="red">0,75l / 0,125l</span></div>
        <ul class="menu__list--vine">
          <li class="menu-list__item">
            <span class="list-item__title">Aurelius, vin. Kovačević</span>
            <span class="list-item__description">(Cabarnet Sauvignon 70%, Merlot 30%)</span>
            <span class="list-item__price">2.400,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Vranac, vin. Vukoje</span>
            <span class="list-item__description">(Vranac 70%, Merlot 20%, Cab. Sauvignon 10%)</span>
            <span class="list-item__price">2.800,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Regent, vin. Aleksandrović</span>
            <span class="list-item__description">(Cabarnet Sauvignon 50%, Merlot 50%)</span>
            <span class="list-item__price">2.800,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Život teče, Zvonko Bogdan</span>
            <span class="list-item__description">(Merlot 50%, Cabarnet Franc 30%, Frankovka 20%)</span>
            <span class="list-item__price">2.800,00 / -</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Crvena vina -->
    <!-- Roze vina -->
    <div class="row menu__row jt_row">
      <h3 class="menu__heading">Roze vina / Rose vine</h3>
      <div class="col-md-6 menu-items__wrapper">
        <div class="quantity__heading"><span class="red">0,75l / 0,125l</span></div>
        <ul class="menu__list--vine">
          <li class="menu-list__item">
            <span class="list-item__title">Rose, vin. Rubin 1l</span>
            <span class="list-item__price">800,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Rose, vin. Rubin</span>
            <span class="list-item__description">(Prokupac 100%)</span>
            <span class="list-item__price">900,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Rose, vin. Tikves</span>
            <span class="list-item__description">(Kratosija 100%)</span>
            <span class="list-item__price">1.400,00 / 240,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <div class="quantity__heading m--hide"><span class="red">0,75l / 0,125l</span></div>
        <ul class="menu__list--vine">
          <li class="menu-list__item">
            <span class="list-item__title">Rose Shiraz, Cavanza, Cile</span>
            <span class="list-item__description">(Cabarnet Sauvignon 80%, Syrah 20%)</span>
            <span class="list-item__price">1.400,00 / 240,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Rose, Primavera vin. Vukoje</span>
            <span class="list-item__description">(Muskat Hamburg 100%)</span>
            <span class="list-item__price">1.800,00 / -</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Rose, vin. Zvonko Bogdan</span>
            <span class="list-item__description">(Merlot 50%, Cabarnet Franc 30%, Frankovka 20%)</span>
            <span class="list-item__price">2.100,00 / -</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Roze vina -->
    <!-- Mala vina i penusava vina -->
    <div class="row menu__row jt_row">
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Mala vina</h3>
        <ul class="menu__list--vine">
          <li class="menu-list__item">
            <span class="list-item__title">Chardonnay vin.Plantaže</span>
            <span class="list-item__description">(0.187)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Vranac, Procorde vin. Plantaže</span>
            <span class="list-item__description">(0.187)</span>
            <span class="list-item__price">280,00</span>
          </li>
        </ul>
      </div>
      <div class="col-md-6 menu-items__wrapper">
        <h3 class="menu__heading">Penušava vina</h3>
        <ul class="menu__list--vine">
          <li class="menu-list__item">
            <span class="list-item__title">André Gallois Brut</span>
            <span class="list-item__description">(0.75)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Prosecco</span>
            <span class="list-item__description">(0.75)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Martini Asti</span>
            <span class="list-item__description">(0.75)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Martini Rose</span>
            <span class="list-item__description">(0.75)</span>
            <span class="list-item__price">280,00</span>
          </li>
          <li class="menu-list__item">
            <span class="list-item__title">Moet & Chandon</span>
            <span class="list-item__description">(0.75)</span>
            <span class="list-item__price">280,00</span>
          </li>
        </ul>
      </div>
    </div>
    <!-- (x) Mala vina i penusava vina -->
  </div>
  <div class="gap--100"></div>
  <!-- (x) Container -->
</section>

<!--        <!-- BEGIN FOOTER -->
<?php
include __DIR__ . '/views/includes/footer.php';
?>
<!--        <!-- END FOOTER -->
<a href="#0" class="cd-top">Top</a>
<!--    <!-- BEGIN Scripts-->
<?php
include __DIR__ . '/views/includes/scripts.php';
?>

<!--<!-- END Scripts -->
</body>
</html>
