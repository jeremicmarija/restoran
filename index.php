<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include __DIR__ . '/views/includes/head.php';
    ?>
</head>

<body>
<?php
    $page = filter_input(INPUT_GET, 'page');
    if (false == $page) {
        $page = 'home';
    }

require_once __DIR__ . '/controllers/' . $page . '.php';

    include __DIR__ . '/views/includes/footer.php';
 ?>
    <a href="#0" class="cd-top">Top</a>
<?php
    include __DIR__. '/views/includes/scripts.php';
?>

<!--<!-- END Scripts -->
</body>
</html>
