<?php
require_once 'email.php';
/**
 * reservation
 */
class Reservation
{
    public $fullname;
    public $email;
    public $date;
    public $time;
    public $textmsg;
    public $phone;
    public $party;
    public $status = true;
    public $message = "";

    function __construct($fullname,$email, $date, $time, $textmsg, $phone, $party)
    {
        $this->fullname = $fullname;
        $this->email = $email;
        $this->date = $date;
        $this->time = $time;
        $this->textmsg = $textmsg;
        $this->phone = $phone;
        $this->party = $party;

    }
    public function SendReservation()
    {
        if (strlen($this->fullname)>150 || strlen($this->fullname)<3) {
            $this->message .= "Ime i prezime je potrebno da budu izmedju 3 i 150 karaktera.";
            $this->status = false;
            return false;
        }
        //radimo proveru emaila
        if(!preg_match('/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/',$this->email)) {
            $this->message .="Neispravan email. Unesite ispravan email.";
            $this->status = false;
            return false;
        }


        $Reservation = new Email();
        $this->fullname = $Reservation->CleanString($this->fullname);
        $this->email = $Reservation->CleanString($this->email);
        $this->date = $Reservation->CleanString($this->date);
        $this->time = $Reservation->CleanString($this->time);
        $this->textmsg = $Reservation->CleanString($this->textmsg);
        $this->phone = $Reservation->CleanString($this->phone);
        $this->party = $Reservation->CleanString($this->party);


        //saljemo email
        $Reservation->EmailTo= "info@restoranpromaja.rs";
        $Reservation->EmailFrom = "info@restoranpromaja.rs";
        $Reservation->Title = "Upit za rezervaciju";
        $Reservation->EmailMessage ="Ime i prezime: ".$this->fullname;
        $Reservation->EmailMessage .="\nEmail korisnika: ".$this->email;
        $Reservation->EmailMessage .="\nDatum: ".$this->date;
        $Reservation->EmailMessage .="\nVreme: ".$this->time;
        $Reservation->EmailMessage .="\nTelefon: ".$this->phone;
        $Reservation->EmailMessage .="\nPovod: ".$this->party;
        $Reservation->EmailMessage .="\nPoruka: ".$this->textmsg;
        // $Reservation->EmailMessage .="\n\nYou received message from website : ".'http://fidgetzsrbija.com';
        $Reservation->SendEmail();
        $this->status = $Reservation->status;
        if ($this->status) {
            $this->message .= "Uspesno ste poslali upit za rezervaciju. Uskoro cemo vas kontaktirati.";
            $this->status = true;
        }else{
            $this->message .= "Poruka nije uspesno poslata, molimo vas da pokusate ponovo ili da nas kontaktirate na info@restoranpromaja.rs.";
            $this->status = false;
        }

        /************Da se posalje i adminu radi provere************/
        $Reservation->EmailTo= "makita.marija@gmail.com";
        $Reservation->SendEmail();

    }

}

?>