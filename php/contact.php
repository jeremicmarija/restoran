<?php
require_once 'email.php';
/**
 * kontakt
 */
class Contact
{
    public $name;
    public $email;
    public $subject;
    public $textmessage;
    public $status = true;
    public $message = "";

    function __construct($name,$email ,$subject,$textmessage)
    {
        $this->name = $name;
        $this->email = $email;
        $this->subject = $subject;
        $this->textmessage = $textmessage;

    }
    public function SendEmail()
    {
        if (strlen($this->name)>150 || strlen($this->name)<3) {
            $this->message .= "Ime i prezime je potrebno da budu izmedju 3 i 150 karaktera.";
            $this->status = false;
            return false;
        }
        //radimo proveru emaila
        if(!preg_match('/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/',$this->email)) {
            $this->message .="Neispravan email. Unesite ispravan email.";
            $this->status = false;
            return false;
        }


        $SlanjeEmaila = new Email();
        $this->name = $SlanjeEmaila->CleanString($this->name);
        $this->email = $SlanjeEmaila->CleanString($this->email);
        $this->subject = $SlanjeEmaila->CleanString($this->subject);
        $this->textmessage = $SlanjeEmaila->CleanString($this->textmessage);


        //saljemo email
        $SlanjeEmaila->EmailTo= "info@restoranpromaja.rs";
        $SlanjeEmaila->EmailFrom = "info@restoranpromaja.rs";
        $SlanjeEmaila->Title = $this->subject;
        $SlanjeEmaila->EmailMessage ="Full name: ".$this->name;
        $SlanjeEmaila->EmailMessage .="\nEmail korisnika: ".$this->email;
        $SlanjeEmaila->EmailMessage .="\nNaslov: ".$this->subject;
        $SlanjeEmaila->EmailMessage .="\nPoruka: ".$this->textmessage;
        $SlanjeEmaila->EmailMessage .="\n\nYou received message from website : ".'http://www.restoranpromaja.rs/';
        $SlanjeEmaila->SendEmail();
        $this->status = $SlanjeEmaila->status;
        if ($this->status) {
            $this->message .= "Uspesno ste poslali poruku. Uskoro cemo vas kontaktirati.";
            $this->status = true;
        }else{
            $this->message .= "Poruka nije uspesno poslata, molimo vas da pokusate ponovo ili da nas kontaktirate na info@restoranpromaja.rs.";
            $this->status = false;
        }

        /************Da se posalje i adminu radi provere************/
        $SlanjeEmaila->EmailTo= "makita.marija@gmail.com";
        $SlanjeEmaila->SendEmail();

    }

}

?>