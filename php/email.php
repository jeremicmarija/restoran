<?php
/**
 * obavezno pozivaj clean_string kako bi uklinio nezeljene stvari iz dobijenih varijabli
 */
class Email
{
    public $EmailTo;
    public $EmailFrom;
    public $Title;
    public $EmailMessage;
    public $EmailHeaders;

    public $message;
    public $status=false;
    function __construct()
    {

    }
    public function CleanString($string) {
        $bad = array("content-type","bcc:","to:","cc:","href"); /* zamenjuje navedene stringove sa "" */
        return str_replace($bad,"",$string);
    }
    public function SendEmail()
    {
        if (!isset($this->EmailHeaders)) {
            $this->EmailHeaders  = 'MIME-Version: 1.0' . "\r\n";
            $this->EmailHeaders .= 'Content-type: text/plain; charset=utf-8' . "\r\n";
            $this->EmailHeaders .='From: '.$this->EmailFrom;
        }

        if(@mail($this->EmailTo, $this->Title, $this->EmailMessage, $this->EmailHeaders)){
            $this->message="Uspesno!";
            $this->status = true;
        }else{
            $this->message="Slanje nije proslo!";
            $this->status = false;
        }
    }
}
?>