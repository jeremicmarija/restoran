$(document).ready(function(){


    //function for send messages - contact on site
    var sendMsg = function(){

        var form = $('#contactform');
        $.ajax({
            type: "POST",
            url: form.attr("action"),
            data: form.serialize(),
            success: function (data) {
                if (data) {
                   console.log(data);
                } else {
                   console.log('not success');
                }
            }
        });
    };

    $('#sendmsg').on('click', function () {
        sendMsg();
    });



    //function to send reservation msg
    var sendReservation = function(){
        var form = $('#reservationform');
        $.ajax({
            type: "POST",
            url: form.attr("action"),
            data: form.serialize(),
            success: function (data) {
                if (data) {
                    $('#res-status').html('<strong>Uspesno ste poslali poruku!</strong> Hvala sto ste nas kontaktirali.');
                    console.log(data);
                } else {
                    $('#res-status').html('<strong>Neuspesno!</strong> Molimo pokusajte ponovo.');
                    console.log('not success');
                }
            }
        });
    }

    $('#sendReservation').on('click', function () {
        sendReservation();
    });

});