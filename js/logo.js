$(document).ready(function(){
   var headerH = $('#header').height();

    $(window).scroll(function() {
        if ($(this).scrollTop() > headerH) { // this refers to window

            $('.logo_elixir').removeClass('logowhite').addClass('logoblack');
        }
        if ($(this).scrollTop() <= headerH) { // this refers to window

            $('.logo_elixir').removeClass('logoblack').addClass('logowhite');
        }
    });

});
